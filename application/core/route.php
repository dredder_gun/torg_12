<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 10.10.2016
 * Time: 2:35
 */

class Route
{

    static function start()
    {

        $controller_name = 'Main';
        $action_name = 'index';

        //уберём $_GET переменные из запроса
        $url = explode('?', $_SERVER['REQUEST_URI']);
        $url = $url[0];

        $routes = explode('/', $url);

        // получаем имя контроллера
        if ( !empty($routes[1]) )
        {
            $controller_name = $routes[1];
        }

        // получаем имя экшена
        if ( !empty($routes[2]) )
        {
            $action_name = $routes[2];
        }

        $model_name = 'Model_'.$controller_name;
        $controller_name = 'Controller_'.$controller_name;
        $action_name = 'action_'.$action_name;

        $model_file = strtolower($model_name).'.php';
        $model_path = "application/models/".$model_file;
        if(file_exists($model_path))
        {
            include "application/models/".$model_file;
        }

        // подцепляем файл с классом контроллера
        $controller_file = strtolower($controller_name).'.php';
        $controller_path = "application/controllers/".$controller_file;

        if(file_exists($controller_path))
        {
            include "application/controllers/".$controller_file;
        }
        else
        {
            /*
            правильно было бы кинуть здесь исключение,
            но для упрощения сразу сделаем редирект на страницу 404
            */
            trigger_error('Такого контроллера нет');
        }

        // создаем контроллер
        $controller = new $controller_name;
        $action = $action_name;

        if(method_exists($controller, $action))
        {
            if ($controller_name == 'cart' && isset($_SESSION['cart_id'])){
                $cart = new Controller_cart($_SESSION['cart_id']);
                $cart->get_cart();
            }
            else{
                $controller->$action();
            }
        }
        else
        {
            //echo 'Passed through the route error <br>';
            // здесь также разумнее было бы кинуть исключение
            Route::ErrorPage404();
        }

    }

    static function start_admin()
    {

        if (isset ($_POST['submit']))
        {
            if ($_POST['username'] == ADMIN_USERNAME
                && $_POST['password'] == ADMIN_PASSWORD)
            {
                $_SESSION['admin_logged'] = true;

                header('Location: ' . SITE_ROOT.'?admin');
                exit();
            }
            else
                echo '<h3 style="color: red; text-align: center;">Неверный имя пользователя и пароль!</h3>';
        }

        $admin_contr = new Admin_controller();
        $admin_contr->init();

    }

    function ErrorPage404()
    {
        $host = 'https://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
    }

}
