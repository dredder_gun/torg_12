<?php

class Controller_Main extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function action_index()
    {

          if(!isset($_SESSION['uniqid'])) {
              $_SESSION['uniqid'] = uniqid('torg_');
              $products_list = '';
          } else{
              $products_list = Model_main::get_product_from_document($_SESSION['uniqid']);
          }

          $this->view->generate($products_list);
    }

}