<?php

class Model_main extends Model
{

    public static function get_all_products()
    {
        $sql = 'SELECT pl.id, pl.articul, pl.quantity, pl.unit, pl.price, pl.nds, pl.counrty FROM product_list pl;';

        return self::GetAll($sql);

    }

    public static function get_product_from_document($id_document)
    {

        $sql = 'SELECT pl.id, pl.articul, pl.quantity, pl.unit, pl.price, pl.nds, pl.counrty, pl.id_document FROM product_list pl WHERE pl.id_document = :inId;';

        $param = array(
            ':inId' => $id_document
        );

        return self::GetAll($sql, $param);
    }

}