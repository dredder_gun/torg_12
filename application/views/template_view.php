<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <title>ТОРГ-12</title>
</head>
<body>
    <div class="container">
        <h1 style="text-align: center;margin-top:40px;">Документ</h1>
        <div class="document_body">
            <div class="up_wrapper">
                <div class="block_one">
                    <p>Дата: <input type="date" name="date_document" value="Выберите дату"></p>
                    <p class="error_data" style="color: red"></p>
                    <p>
                        Поставщик:
                        <select name="our_organization" id="our_org">
                            <option disabled>Выберите собственную организацию</option>
                            <option value="our">ООО Наша организация</option>
                            <option value="">Компания 1</option>
                        </select>
                    </p>
                </div>

                <div class="block_two">
                    <label for="number_nacl">
                        Номер накладной
                    </label>
                    <?=$_SESSION['uniqid']?>
                    <p>
                        <label for="our_organization">Плательщик:</label>
                        <select name="counter_org" id="counter_org">
                            <option disabled>Выберите контагента</option>
                            <option value="our">ООО Организация контрагента</option>
                            <option value="">Партнёр 1</option>
                            <option value="">Партнёр 2</option>
                            <option value="">Партнёр 3</option>
                        </select>
                    </p>
                </div>
            </div>

            <form class="form_new_articul form-inline">

                <input type="submit" value="Добавить" class="btn btn-primary btn-articul">
                <div class="form-group">
                    <label for="articul">Артикул</label>
                    <input class="form-control" name="articul" type="text" placeholder="Введите артикул">
                    <p class="error_message error_message0"></p>
                </div>
                <div class="form-group">
                    <label for="quantity">Количество</label>
                    <input class="form-control" name="quantity" type="number" placeholder="Введите количество">
                    <p class="error_message error_message1"></p>
                </div>
                <div class="form-group">
                    <label for="unit">Единица измерения</label>
                    <input class="form-control" name="unit" type="text" placeholder="Введите единицы измерения">
                    <p class="error_message error_message2"></p>
                </div>
                <div class="form-group">
                    <label for="nds">Цена</label>
                    <input class="form-control" name="price" type="number" placeholder="Введите цену">
                    <p class="error_message error_message3"></p>
                </div>
                <input type="number" value="10" name="nds" hidden>
                <div class="form-group">
                    <label for="counrty">Страна</label>
                    <input class="form-control" name="counrty" type="text" placeholder="Введите страну">
                    <p class="error_message error_message4"></p>
                </div>
                <input type="text" value="<?=$_SESSION['uniqid']?>" name="id_document" hidden>

            </form>
            <p class="success_result" style="color: green;"></p>
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th class="id_col">ID документа</th>
                    <th>Артикул</th>
                    <th>Количество</th>
                    <th>Ед.измер</th>
                    <th>Цена</th>
                    <th>Страна</th>
                    <th class="del_col">Удалить?</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $i = 0;
                    foreach ($product_list as $product){ ?>
                    <tr>
                        <th scope="row"><?=$i += 1?></th>
                        <td class="id_col"><?=$_SESSION['uniqid']?></td>
                        <td><?=$product['articul']?></td>
                        <td><?=$product['quantity']?></td>
                        <td><?=$product['unit']?></td>
                        <td><?=$product['price']?></td>
                        <td><?=$product['counrty']?></td>
                        <th class="del_col"><button class="delete_prod" data-id_good="<?=$product['id']?>">X</button></th>
                    </tr>
                <?php }
                ?>

                </tbody>
            </table>
            <div class="print_button">
                <button class="print_doc">ТОРГ-12</button>
                <button class="save_doc">Сохранить и создать новый</button>
            </div>
        </div>
    </div>
</body>

<script src='<?=SITE_ROOT?>/bower_components/jquery/dist/jquery.js'></script>
<script src='<?=SITE_ROOT?>/bower_components/bootstrap/dist/js/bootstrap.min.js'></script>
<script src='<?=SITE_ROOT?>/js/ajax_requets.js'></script>
<script src='<?=SITE_ROOT?>/actions.js'></script>

</html>