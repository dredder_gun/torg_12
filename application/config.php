<?php

define('ROOT_PATH', dirname(dirname(__FILE__)));
define('SITE_ROOT', '//'.$_SERVER['SERVER_NAME']);

define('DESCRIPTION_LENGTH', 30);
define('PRODUCT_ON_PAGE', 6);
define('START_WITH', 0);

define('GOODS_IMAGES', IMAGES_DIR.'goods/');
define('GOODS_IMAGES_TYPES', array('image/gif', 'image/png', 'image/jpeg'));
define('GOODS_IMAGES_SIZE', 1024000);

// Параметры соединения с базой данных
define('DB_PERSISTENCY', 'true');
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'torg');
define('PDO_DSN', 'mysql:host=' . DB_SERVER . ';dbname=' . DB_DATABASE);