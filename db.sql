--
-- Скрипт сгенерирован Devart dbForge Studio for MySQL, Версия 7.2.53.0
-- Домашняя страница продукта: http://www.devart.com/ru/dbforge/mysql/studio
-- Дата скрипта: 07.12.2016 3:44:17
-- Версия сервера: 5.7.13
-- Версия клиента: 4.1
--


--
-- Отключение внешних ключей
--
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

--
-- Установить режим SQL (SQL mode)
--
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

--
-- Установка кодировки, с использованием которой клиент будет посылать запросы на сервер
--
SET NAMES 'utf8';

--
-- Установка базы данных по умолчанию
--
USE torg;

--
-- Описание для таблицы company_list
--
DROP TABLE IF EXISTS company_list;
CREATE TABLE company_list (
  id INT(11) NOT NULL AUTO_INCREMENT,
  INN INT(11) DEFAULT NULL,
  name VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX id (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы individual_entitys
--
DROP TABLE IF EXISTS individual_entitys;
CREATE TABLE individual_entitys (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  first_name VARCHAR(255) DEFAULT NULL,
  second_name VARCHAR(255) DEFAULT NULL,
  third_name VARCHAR(255) DEFAULT NULL,
  INN_date DATE DEFAULT NULL,
  passport INT(11) DEFAULT NULL,
  passport_date_release DATE DEFAULT NULL,
  passport_whome_release VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX id (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы legal_entitys
--
DROP TABLE IF EXISTS legal_entitys;
CREATE TABLE legal_entitys (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  KPP INT(11) DEFAULT NULL,
  OKATO INT(11) DEFAULT NULL,
  legal_address VARCHAR(255) DEFAULT NULL,
  fact_address VARCHAR(255) DEFAULT NULL,
  contract VARCHAR(255) DEFAULT NULL,
  phone VARCHAR(255) DEFAULT NULL,
  `e-mail` VARCHAR(255) DEFAULT NULL,
  data VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX id (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 1
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

--
-- Описание для таблицы product_list
--
DROP TABLE IF EXISTS product_list;
CREATE TABLE product_list (
  id INT(11) NOT NULL AUTO_INCREMENT,
  articul VARCHAR(255) DEFAULT NULL,
  quantity INT(11) DEFAULT NULL,
  unit VARCHAR(255) DEFAULT NULL,
  price DECIMAL(19, 2) DEFAULT NULL,
  nds DECIMAL(10, 2) DEFAULT NULL,
  counrty VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX id (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 15
AVG_ROW_LENGTH = 1638
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;

DELIMITER $$

--
-- Описание для процедуры add_new_goods
--
DROP PROCEDURE IF EXISTS add_new_goods$$
CREATE DEFINER = 'root'@'%'
PROCEDURE add_new_goods(
  IN articul varchar(255),
  IN quantity int,
  IN unit varchar(255),
  IN price float,
  IN nds float,
  IN country varchar(255)
  )
BEGIN
  INSERT INTO product_list (articul, quantity, unit, price, nds, counrty) VALUES (articul, quantity, unit, price, nds, country);
END
$$

--
-- Описание для процедуры delete_product
--
DROP PROCEDURE IF EXISTS delete_product$$
CREATE DEFINER = 'root'@'%'
PROCEDURE delete_product(
  IN inIDproduct int
  )
BEGIN
  DELETE FROM product_list WHERE id = inIDproduct;
END
$$

--
-- Описание для процедуры get_products
--
DROP PROCEDURE IF EXISTS get_products$$
CREATE DEFINER = 'root'@'%'
PROCEDURE get_products()
BEGIN
  SELECT pl.id, pl.articul, pl.quantity, pl.unit, pl.price, pl.nds, pl.counrty FROM product_list pl;
END
$$

DELIMITER ;

--
-- Вывод данных для таблицы company_list
--

-- Таблица torg.company_list не содержит данных

--
-- Вывод данных для таблицы individual_entitys
--

-- Таблица torg.individual_entitys не содержит данных

--
-- Вывод данных для таблицы legal_entitys
--

-- Таблица torg.legal_entitys не содержит данных

--
-- Вывод данных для таблицы product_list
--
INSERT INTO product_list VALUES
(1, 'Ножницы канцелярские', 10, 'шт', 5.00, 10.00, 'Китай'),
(2, 'Ручка шариковая, из пластика', 15, 'шт', 4.00, 10.00, 'РФ'),
(3, 'Ручка гелевая, красная', 13, 'шт', 7.00, 10.00, 'РФ'),
(4, 'Карандаш деревянный,  синий', 8, 'шт', 10.00, 20.00, 'Франция'),
(5, 'Скотч, 5см', 20, 'шт', 18.00, 20.00, 'РФ'),
(6, 'Стэплер черный', 3, 'шт', 40.00, 10.00, 'РФ'),
(11, 'Нотная тетрадь', 9, 'книга', 40.00, 10.00, NULL),
(12, 'Карандаш', 50, 'шт', 4.00, 10.00, NULL),
(13, 'Прищепка для бумаги', 15, 'шт', 7.00, 10.00, NULL),
(14, 'Детский паззл', 18, 'коробка', 80.00, 10.00, NULL);

--
-- Восстановить предыдущий режим SQL (SQL mode)
--
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

--
-- Включение внешних ключей
--
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;